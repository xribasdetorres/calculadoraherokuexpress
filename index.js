var express = require('express');
var app = express();
var port = process.env.PORT;

app.set('views', __dirname + '/views');
app.set('view enjine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.get('/',function(req,res)
{
    res.sendFile( __dirname + '/views/index.html');
}
);
app.get('/suma', loadParams, function (req,res)
{
    require(__dirname + '/views/suma.js').get(req, res);
}
);
app.get('/resta', loadParams, function (req,res)
{
    require(__dirname + '/views/resta.js').get(req, res);
}
);
app.get('/multiplicacio', loadParams, function (req,res)
{
    require(__dirname + '/views/Multi.js').get(req, res);
}
);
app.get('/divisio', loadParams, function (req,res)
{
    require(__dirname + '/views/Div.js').get(req, res);
}
);

app.listen(port);
function loadParams(req, res, next) {
   
    var url = require('url');
    req.requrl = url.parse(req.url, true);
    req.a = (req.requrl.query.a && (!isNaN(req.requrl.query.a)) ? new Number(req.requrl.query.a):
    NaN);
    req.b = (req.requrl.query.b && (!isNaN(req.requrl.query.b)) ? new Number(req.requrl.query.b):
    NaN);
    if (next) next();
}